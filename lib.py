import markovify
import json
import re

def strip_mentions(s):
	return re.sub(r"@(\S+)", r"[at]\1", re.sub(r"^(@\S+ )*", "", s))

msg = []
with open("data.json") as f:
	for x in json.load(f):
		msg.append(strip_mentions(re.sub(r"(\s\d+)+$", "", re.sub(r"[\0-\x19]", "", x.strip()))))

txt = markovify.Text(msg)

if __name__ == "__main__":
	print(txt.make_short_sentence(70))
