import requests
from lib import txt
import json

with open("config.json") as f:
	config = json.load(f)

cookies = {
	"__Host-pleroma_key": config["pleroma_key"],
}
form = {
	"status": txt.make_short_sentence(500),
	"source": config["source"],
	"visibility": config["visibility"],
	"content_type": "text/plain",
	"media_ids": "",
}
class Auth(requests.auth.AuthBase):
	def __call__(self, r):
		r.headers["Authorization"] = config["authorization"]
		return r
r = requests.post("https://%s/api/statuses/update.json" % config["base"], auth=Auth(), cookies=cookies, data=form)
print(r.status_code)
print(r.text)
